package com.example.offline.model;

/**
 * Created by Abhishek on 12/4/2017.
 */

public class DataWrapper<T> {
    private T data;
    private String error;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}