package com.example.offline.di;


import com.example.offline.view.fragment.NewsFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentBuildersModule {
    @ContributesAndroidInjector(modules = CategoryFragmentModule.class)
    abstract NewsFragment contributeProjectFragment();
}
