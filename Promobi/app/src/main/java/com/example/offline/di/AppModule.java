package com.example.offline.di;

import android.content.Context;

import com.example.offline.App;
import com.example.offline.data.datasource.LocalNewsDataStore;
import com.example.offline.data.datasource.LocalNewsStore;
import com.example.offline.data.datasource.dao.AppDatabase;
import com.example.offline.data.datasource.dao.NewsInfoDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * This is where you will inject application-wide dependencies.
 */
@Module
public class AppModule {

    @Provides
    Context provideContext(App application) {
        return application.getApplicationContext();
    }


    @Singleton
    @Provides
    NewsInfoDao provideCategoryInfoDao(Context context) {
        return AppDatabase.getInstance(context).newsInfoDao();
    }

    @Singleton
    @Provides
    LocalNewsStore provideLocalCategoryInfoRepository(NewsInfoDao categoryInfoDao) {
        return new LocalNewsDataStore(categoryInfoDao);
    }

}
