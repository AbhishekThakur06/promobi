package com.example.offline.data.datasource;

import com.example.offline.data.datasource.dao.NewsInfoDao;
import com.example.offline.model.entity.News;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Abhishek on 11/30/2017.
 */

public class LocalNewsDataStore implements LocalNewsStore {
    private final NewsInfoDao categoryInfoDao;
    public LocalNewsDataStore(NewsInfoDao categoryInfoDao) {
        this.categoryInfoDao = categoryInfoDao;
    }

    @Override
    public void putNews(List<News> categoryInfoList) {

         categoryInfoDao.add(categoryInfoList) ;
    }

    @Override
    public Flowable<List<News>> getNews() {

        return categoryInfoDao.getNews();
    }

    @Override
    public Flowable<List<News>> getNews(long date) {
        return categoryInfoDao.getNews(date);
    }

    @Override
    public List<News> getNewsAll(long date) {
        return categoryInfoDao.getNewsAll(date);
    }

    @Override
    public void clearTable() {
        categoryInfoDao.clearTable(0);
    }
}
