package com.example.offline.data.datasource.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.offline.model.entity.News;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Abhishek on 11/30/2017.
 */
@Dao
public interface NewsInfoDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void add(List<News> newsList);

    @Query("SELECT * FROM News Limit 10")
    Flowable<List<News>> getNews();

   @Query("DELETE FROM News where id != :idLimit")
    void clearTable(int idLimit);

    @Query("SELECT * FROM News where publish_date_long < :date ORDER BY  publish_date_long DESC LIMIT 10")
    Flowable<List<News>> getNews(long date);

    @Query("SELECT * FROM News where publish_date_long < :date ORDER BY  publish_date_long DESC ")
    List<News> getNewsAll(long date);
}