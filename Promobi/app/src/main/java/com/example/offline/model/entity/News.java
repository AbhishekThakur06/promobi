package com.example.offline.model.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;

/**
 * Created by Abhishek on 12/28/2017.
 */
@Entity
public class News {

    @Expose
    @ColumnInfo(name = "web_url")
    private String webUrl;

    @Expose
    @ColumnInfo(name = "snippet")
    private String snippet;

    @Expose
    @ColumnInfo(name = "multimedia")
    private String multimedia = null;

    @Expose
    @ColumnInfo(name = "headline")
    private String headline;

    @Expose
    @ColumnInfo(name = "publish_date")
    private String pubDate;

    @Expose
    @ColumnInfo(name = "publish_date_long")
    private long pubDateLong;

    @Expose
    @ColumnInfo(name = "id")
    @PrimaryKey
    private String id;

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public String getSnippet() {
        return snippet;
    }

    public void setSnippet(String snippet) {
        this.snippet = snippet;
    }

    public String getMultimedia() {
        return multimedia;
    }

    public void setMultimedia(String multimedia) {
        this.multimedia = multimedia;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getPubDateLong() {
        return pubDateLong;
    }

    public void setPubDateLong(long pubDateLong) {
        this.pubDateLong = pubDateLong;
    }
}
