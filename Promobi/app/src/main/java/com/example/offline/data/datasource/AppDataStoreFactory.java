package com.example.offline.data.datasource;

import android.content.Context;
import android.support.annotation.NonNull;

import com.example.offline.data.net.NYTService;
import com.example.offline.data.net.ServiceFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
/**
 *Factory class to provide all the data stores local as well as cloud
 *
 */

@Singleton
public class AppDataStoreFactory {

    private final Context context;
    private LocalNewsStore localNewsStore;


    @Inject
    AppDataStoreFactory(@NonNull Context context, LocalNewsStore localNewsStore) {
        this.context = context.getApplicationContext();
        this.localNewsStore = localNewsStore;
    }


    /**
     * Create {@link RemoteDataStore} to retrieve data from the Cloud.
     */
    public RemoteDataStore createCloudDataStoreForNews() {
        NYTService service = ServiceFactory.createRetrofitService(NYTService.class, NYTService.SERVICE_ENDPOINT);
        return new CloudUserDataStore(service);
    }

    /**
     *
     * @return to add and retrieve data related to Categories of product
     */
    public LocalNewsStore getNewsStore() {
        return localNewsStore;
    }

   }
