package com.example.offline.data.net;

import com.example.offline.model.newsmodel.NewsData;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Abhishek on 12/28/2017.
 */

public interface NYTService {
    String SERVICE_ENDPOINT = "https://api.nytimes.com/svc/search/v2/";

    @GET("articlesearch.json")
    Observable<NewsData> getNews(@Query("api_key") String apikey, @Query("page") int page, @Query("sort") String sort);
}