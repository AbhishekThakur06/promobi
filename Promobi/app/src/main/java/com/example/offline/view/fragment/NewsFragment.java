package com.example.offline.view.fragment;

import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.offline.R;
import com.example.offline.di.Injectable;
import com.example.offline.model.DataWrapper;
import com.example.offline.model.entity.News;
import com.example.offline.view.EndlessRecyclerOnScrollListener;
import com.example.offline.view.adapter.NewsListAdapter;
import com.example.offline.viewmodel.AppViewModelFactory;
import com.example.offline.viewmodel.NewsViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;

public class NewsFragment extends LifecycleFragment implements Injectable {
    @Inject
    AppViewModelFactory viewModelFactory;

    @BindView(R.id.comments_recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.progress)
    ProgressBar progress;
    private NewsListAdapter recyclerViewAdapter;

    private NewsViewModel viewModel;

    private LifecycleRegistry registry = new LifecycleRegistry(this);

    private LayoutInflater inflater;
    @Override
    public LifecycleRegistry getLifecycle() {
        return registry;
    }

    EndlessRecyclerOnScrollListener  onScrollListener;
    boolean mLoading =false;
    int currentPage = 0;
    private final CompositeDisposable disposable = new CompositeDisposable();
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        // Inflate this data binding layout
        View view = inflater.inflate(R.layout.fragment_category, container, false);
        ButterKnife.bind(this, view);
        this.inflater = inflater;
        initRecyclerView();

        // Create and set the adapter for the RecyclerView.
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(NewsViewModel.class);
        progress.setVisibility(View.VISIBLE);
        viewModel.categories().observe(this, this::handleStreamOfCategory);
        //viewModel.rankings().observe(this, this::updateRankings);

    }
    /** Creates project fragment for specific project ID */
    public static NewsFragment forProject(String projectID) {
        NewsFragment fragment = new NewsFragment();
        Bundle args = new Bundle();
        //args.putString(KEY_PROJECT_ID, projectID);
        fragment.setArguments(args);

        return fragment;
    }

    private void initRecyclerView() {
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager recyclerViewLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(recyclerViewLayoutManager);

        recyclerViewAdapter = new NewsListAdapter(new ArrayList<>() );
        recyclerView.setAdapter(recyclerViewAdapter);
        onScrollListener = new EndlessRecyclerOnScrollListener(recyclerViewLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                viewModel.getNews(page+1);
            }
        };
        recyclerView.addOnScrollListener(onScrollListener);
    }


    private void handleStreamOfCategory(DataWrapper<List<News>> dataWrapper) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progress.setVisibility(View.GONE);
                if(dataWrapper.getError() != null) {
                    Toast.makeText(getActivity(), dataWrapper.getError(), Toast.LENGTH_SHORT).show();
                } else {
                    recyclerViewAdapter.updateCommentList(dataWrapper.getData());
                    mLoading = false;
                }
            }
        });

    }
}
