package com.example.offline.domain;

import com.example.offline.model.entity.News;

import java.util.List;

import io.reactivex.subscribers.ResourceSubscriber;

/**
 * Created by Abhishek on 11/30/2017.
 */

public interface NewsRepository {
    /**
     * emit a list of News and notify the consumer
     */
    void news(ResourceSubscriber<List<News>> consumer, String apiKey, int page, long lastDate);


}
