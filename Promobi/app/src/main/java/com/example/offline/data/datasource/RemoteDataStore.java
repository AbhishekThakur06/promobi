package com.example.offline.data.datasource;


import com.example.offline.model.newsmodel.NewsData;

import io.reactivex.Observable;

/**
 * Interface that represents a data store from where data is retrieved.
 */
public interface RemoteDataStore {
  Observable<NewsData> getNews(String apiKey, int page, String sort);

}
