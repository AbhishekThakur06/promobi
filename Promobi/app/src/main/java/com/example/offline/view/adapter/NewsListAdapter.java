package com.example.offline.view.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.offline.R;
import com.example.offline.model.entity.News;
import com.example.offline.view.callback.CategoryClickCallback;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.ViewHolder> {

    private final List<News> newsList;
    private final CategoryClickCallback categoryClickCallback;
    private Context mContext;
    public NewsListAdapter(List<News> newsList) {
        this.newsList = newsList;
        this.categoryClickCallback = null;
    }
    public NewsListAdapter(List<News> newsList, CategoryClickCallback categoryClickCallback) {
        this.newsList = newsList;
        this.categoryClickCallback = categoryClickCallback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view =  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_row, parent, false);
        mContext = parent.getContext();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final News news = newsList.get(position);
        /*if (comment.isSyncPending()) {
            holder.commentText.setTextColor(Color.LTGRAY);
        } else {
        */    holder.commentText.setTextColor(Color.BLACK);
        //}
        holder.commentText.setText(news.getHeadline());
        holder.newsLink.setText(news.getWebUrl());
        holder.newsLink.setTextColor(Color.LTGRAY);
        Glide.with(mContext).load(news.getMultimedia())
                .placeholder(R.mipmap.ic_wifi)
                .into(holder.newsImage);
        // holder.commentText.setOnClickListener(new OnCategoryClickListener(newsList.get(position)));
    }

    @Override
    public int getItemCount() {
        return newsList == null ? 0 : newsList.size();
    }

    public void updateCommentList(List<News> newsList) {
        Timber.d("Got new newsList " + newsList.size());
        this.newsList.clear();
        this.newsList.addAll(newsList);
        notifyDataSetChanged();
    }

    /**
     * View holder for shopping list items of this adapter
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.comment)
        TextView commentText;
        @BindView(R.id.news_link)
        TextView newsLink;
        @BindView(R.id.news_image)
        ImageView newsImage;
        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

    class OnCategoryClickListener implements View.OnClickListener {
        News news;
        OnCategoryClickListener(News news) {
            this.news = news;
        }
        @Override
        public void onClick(View view) {
            categoryClickCallback.onClick(news);
        }
    };


}
