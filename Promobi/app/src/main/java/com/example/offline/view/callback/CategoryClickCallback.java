package com.example.offline.view.callback;

import com.example.offline.model.entity.News;

public interface CategoryClickCallback {
    void onClick(News news);
}
