package com.example.offline.data.datasource;

import com.example.offline.model.entity.News;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Abhishek on 11/30/2017.
 */

public interface LocalNewsStore {
    void putNews(List<News> newsList);
    Flowable<List<News>> getNews();
    Flowable<List<News>> getNews(long date);
    List<News> getNewsAll(long date);
    void clearTable();
}
