package com.example.offline.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.example.offline.App;
import com.example.offline.domain.GetNewsUseCase;

public class AppViewModelFactory implements ViewModelProvider.Factory {
    private final GetNewsUseCase getCategoriesUseCase;
    private App context;
    public AppViewModelFactory(App context, GetNewsUseCase getCategoriesUseCase) {
        this.getCategoriesUseCase = getCategoriesUseCase;
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(NewsViewModel.class)) {
            return (T) new NewsViewModel(context, getCategoriesUseCase);
        }
            throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
