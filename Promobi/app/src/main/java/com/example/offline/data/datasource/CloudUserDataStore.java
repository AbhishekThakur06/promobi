
package com.example.offline.data.datasource;

import com.example.offline.data.net.NYTService;
import com.example.offline.data.net.ServiceFactory;
import com.example.offline.model.newsmodel.NewsData;

import io.reactivex.Observable;

/**
 * {@link RemoteDataStore} implementation based on connections to the api (Cloud).
 */
class CloudUserDataStore implements RemoteDataStore {

  //private final RestApi restApi;
  private NYTService service;

  /**
   * Construct a {@link RemoteDataStore} based on connections to the api (Cloud).
   *
   *
   *
   */
  CloudUserDataStore(NYTService service) {
    //this.restApi = restApi;
    this.service = service;
  }

  @Override
  public Observable<NewsData> getNews(String apiKey, int page, String sort) {
    NYTService service = ServiceFactory.createRetrofitService(NYTService.class, NYTService.SERVICE_ENDPOINT);
    return service.getNews(apiKey, page, sort);
  }

}
