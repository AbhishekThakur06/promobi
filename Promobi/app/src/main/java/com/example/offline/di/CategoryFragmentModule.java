package com.example.offline.di;

import com.example.offline.App;
import com.example.offline.data.NewsDataRepository;
import com.example.offline.domain.GetNewsUseCase;
import com.example.offline.viewmodel.AppViewModelFactory;

import dagger.Module;
import dagger.Provides;

/**
 * Define CommentsActivity-specific dependencies here.
 */
@Module
public class CategoryFragmentModule {
    @Provides
    AppViewModelFactory provideCommentsViewModelFactory(App context, GetNewsUseCase getCategoriesUseCase) {
        return new AppViewModelFactory(context,getCategoriesUseCase);
    }


    @Provides
    GetNewsUseCase provideGetCategoriesUseCase(NewsDataRepository categoriesDataRepository) {
        return new GetNewsUseCase(categoriesDataRepository);
    }
}
