package com.example.offline.data;

import com.example.offline.data.datasource.AppDataStoreFactory;
import com.example.offline.domain.NewsRepository;
import com.example.offline.model.entity.News;
import com.example.offline.model.newsmodel.Doc;
import com.example.offline.model.newsmodel.NewsData;
import com.example.offline.model.newsmodel.NewsImage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.BackpressureStrategy;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.ResourceSubscriber;

/**
 * Created by Abhishek on 11/30/2017.
 */

public class NewsDataRepository implements NewsRepository {
    public final static String NYTIMES_BASE_URI = "http://www.nytimes.com/";
    AppDataStoreFactory dataStoreFactory;
    private final CompositeDisposable disposables;

    @Inject
    NewsDataRepository(AppDataStoreFactory dataStoreFactory) {
        this.dataStoreFactory = dataStoreFactory;
        disposables = new CompositeDisposable();

    }

    @Override
    public void news(ResourceSubscriber<List<News>> consumer, String apiKey, int page, long lastDate) {
       disposables.add(dataStoreFactory.createCloudDataStoreForNews().getNews(apiKey, page, "newest")
                        .map(new Function<NewsData, List<News>>() {
                            @Override
                            public List<News> apply(NewsData newsData) throws Exception {
                                return mapDataToDao(newsData);
                            }
                        }).toFlowable(BackpressureStrategy.BUFFER)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(consumer));


      /*  dataStoreFactory.createCloudDataStoreForNews().getNews(apiKey, page, "newest")
                    .map(new Function<NewsData, List<News>>() {
            @Override
            public List<News> apply(NewsData newsData) throws Exception {
                return mapDataToDao(newsData);

            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(new DisposableObserver<List<News>>() {
                    @Override
                    public void onNext(List<News> data) {
                        //clearAllTable();
                        if (data.size() > 0) {
                            disposables.add(dataStoreFactory.getNewsStore().getNews(data.get(0).getPubDateLong())
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribeWith(consumer));
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        //consumer.onError(e);
                        Timber.i("Exception in fetching data", e);
                        disposables.add(dataStoreFactory.getNewsStore().getNews(lastDate)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeWith(consumer));
                    }

                    @Override
                    public void onComplete() {
                        Timber.i("complete");
                    }
                });*/

    }



    private List<News> mapDataToDao(NewsData data) {
        if (data != null) {
            int size = data.getResponse().getDocs().size();
            List<News> newsList = new ArrayList<>();

            for (int i = 0; i < size; i++) {
                Doc doc = data.getResponse().getDocs().get(i);
                News news = new News();
                news.setId(doc.getId());
                news.setHeadline(doc.getHeadline().getMain());
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:sszzzz");
                try {
                  news.setPubDateLong(simpleDateFormat.parse(doc.getPubDate()).getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                news.setPubDate(doc.getPubDate());
                news.setSnippet(doc.getSnippet());
                news.setWebUrl(doc.getWebUrl());/*2017-12-29T08:42:30+0000*/
                if (doc.getMultimedia() != null && doc.getMultimedia().size() > 0) {
                    news.setMultimedia(getArticleThumbnailUrl(doc));
                }
                newsList.add(news);
            }
            dataStoreFactory.getNewsStore()
                    .putNews(newsList);
           List<News> newsNew = dataStoreFactory.getNewsStore()
                    .getNewsAll(0);
            List<News> newsNew2 = dataStoreFactory.getNewsStore()
                    .getNewsAll(newsList.get(0).getPubDateLong());
            return newsList;
        }
        return null;
    }

    private void clearAllTable() {
       /* dataStoreFactory.getNewsStore()
                .clearTable();
  */
    }
    private List<News> getNews( List<News> newsList) {
        return newsList;
    }
    public String getArticleThumbnailUrl(Doc doc) {
        String thumbnailUrl = "";

        for (NewsImage m : doc.getMultimedia()) {
            if (m.getType().equals("image") && m.getSubtype().equals("xlarge")) {
                thumbnailUrl = NYTIMES_BASE_URI + m.getUrl();
                break;
            }
        }
        return thumbnailUrl;
    }
}
