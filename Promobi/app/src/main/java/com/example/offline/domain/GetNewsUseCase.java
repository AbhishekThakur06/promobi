package com.example.offline.domain;

import com.example.offline.model.entity.News;

import java.util.List;

import io.reactivex.subscribers.ResourceSubscriber;

/**
 * Created by Abhishek on 11/30/2017.
 */

public class GetNewsUseCase {

    private final NewsRepository newsRepository;

    public GetNewsUseCase(NewsRepository newsRepository) {
        this.newsRepository = newsRepository;
    }

    public void getNews(ResourceSubscriber<List<News>> consumer, String apiKey, int page, long lastDate) {
        newsRepository.news(consumer, apiKey, page, lastDate);
    }
}
