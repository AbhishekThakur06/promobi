package com.example.offline.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.offline.App;
import com.example.offline.domain.GetNewsUseCase;
import com.example.offline.domain.exception.DefaultErrorBundle;
import com.example.offline.domain.exception.ErrorBundle;
import com.example.offline.model.DataWrapper;
import com.example.offline.model.entity.News;
import com.example.offline.view.exception.ErrorMessageFactory;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.subscribers.ResourceSubscriber;
import timber.log.Timber;

public class NewsViewModel extends ViewModel {


    private final GetNewsUseCase getCategoriesUseCase;
    private final CompositeDisposable disposables = new CompositeDisposable();
    private MutableLiveData<DataWrapper<List<News>>> categoryLiveData = new MutableLiveData<>();
    private MutableLiveData<List<String>> rankingLiveData = new MutableLiveData<>();
    private boolean loadComplete = false;
    private boolean fetch = false;
    private App context;

    public NewsViewModel(App context, GetNewsUseCase getCategoriesUseCase) {
        this.getCategoriesUseCase = getCategoriesUseCase;
        this.context = context;
        loadCategories();
    }

    @Override
    protected void onCleared() {
        disposables.clear();
    }

    public LiveData<DataWrapper<List<News>>> categories() {
        return categoryLiveData;
    }

    public LiveData<List<String>> rankings() {
        return rankingLiveData;
    }

    void loadCategories() {
        getCategoriesUseCase.getNews(new CategoryObserver(),"a02fa66561a24bf193834808f337a197", 1 , 0l);
    }


    private final class RankingObserver implements Consumer<List<String>> {

        @Override
        public void accept(List<String> ranking) throws Exception {
            Timber.i("categoryInfo found", "");
            rankingLiveData.setValue(ranking);
        }
    }

    private final class CategoryObserver extends ResourceSubscriber<List<News>> {
        @Override
        public void onNext(List<News> newsList) {
            Timber.i("categoryInfo found", "");
            /*if(!validateData(newsList)) {
                return;
            }*/
            DataWrapper<List<News>> dataWrapper = new DataWrapper<>();
            dataWrapper.setData(newsList);
           // categoryLiveData.setValue(dataWrapper);
            fetch = false;
            if(dataWrapper.getData() == null || dataWrapper.getData().isEmpty()) {
                loadComplete = true;
            } else {
                if(categoryLiveData.getValue() != null && categoryLiveData.getValue().getData()!= null) {
                    List<News> news = new ArrayList<>();
                    news.addAll(categoryLiveData.getValue().getData());
                    news.addAll(dataWrapper.getData());
                    dataWrapper.setData(news);
                    categoryLiveData.setValue(dataWrapper);
                } else {
                    dataWrapper.setData(newsList);
                    categoryLiveData.setValue(dataWrapper);
                }
            }
        }

        @Override
        public void onError(Throwable e) {
            DataWrapper<List<News>> dataWrapper = new DataWrapper<>();
            dataWrapper.setError(NewsViewModel.this.getErrorMessage(new DefaultErrorBundle((Exception) e)));
            categoryLiveData.setValue(dataWrapper);
        }

        @Override
        public void onComplete() {

        }

    }
    private boolean validateData(List<News> newsList) {
        try {
            if (categoryLiveData.getValue().getData() == null || categoryLiveData.getValue().getData().isEmpty())
                return true;
            if (categoryLiveData.getValue().getData().get(categoryLiveData.getValue().getData().size() - 1).getPubDate().equals(newsList.get(newsList.size() - 1).getPubDate())) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    private String getErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(context,
                errorBundle.getException());
            return errorMessage;
        //this.viewDetailsView.showError(errorMessage);
    }

    public void getNews(int currentPage) {
        if (!loadComplete && !fetch) {
            fetch = true;
            getCategoriesUseCase.getNews(new CategoryObserver(),"a02fa66561a24bf193834808f337a197", currentPage, categoryLiveData.getValue().getData().get(categoryLiveData.getValue().getData().size() -1).getPubDateLong() );
        }
    }

}
